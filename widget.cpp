#include "widget.h"
#include <QtCore/QRegExp>
#include <QtCore/QSet>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtGui/QPainter>
#include <QtGui/QFileDialog>
#include <QtGui/QPicture>

namespace {
    const QPoint StartPoint = QPoint(10, 45);
    const QSize ImageSize = QSize(410, 260);
} // anonymous namespace

Widget::Widget(QWidget *parent)
    : QWidget(parent), m_codePixmap(ImageSize)
{
    m_codePixmap.fill(Qt::white);
    ui.setupUi(this);
    setupTables();
    connectSignals();
    setValidator("EAN-13");
}

void Widget::calcEAN13(QString code)
{
    int bufer = 0;
    int crc;
    int country;
    int number;
    QString countryRule;
    QString ean13;

    for (int i = 0; i < 12; ++i) {
        if (i % 2 == 0)
            bufer += code.at(i).digitValue();
        else
            bufer += 3*code.at(i).digitValue();
    }
    crc = 10 - bufer%10;
    code.push_back(QString::number(crc, 10));
    ui.codeEdit->setText(code);

    country = code.at(0).digitValue();
    code.remove(0, 1);
    countryRule.append(m_countryTable.at(country));
    countryRule.append("CCCCCC");
    ean13.append("101");
    for (int i = 0; i < 12; ++i) {
        number = code.at(i).digitValue();
        if (countryRule.at(i) == 'A') {
            if (i == 6) ean13.append("01010");
            ean13.append(m_aCodesTable.at(number));
        }
        if (countryRule.at(i) == 'B') {
            if (i == 6) ean13.append("01010");
            ean13.append(m_bCodesTable.at(number));
        }
        if (countryRule.at(i) == 'C') {
            if (i == 6) ean13.append("01010");
            ean13.append(m_cCodesTable.at(number));
        }
    }
    ean13.append("101");
    m_ean13 = ean13;
}

void Widget::calcEAN8(QString code)
{
    int bufer = 0;
    int crc;
    int number;
    QString countryRule;
    QString ean8;

    for (int i = 0; i < 7; ++i) {
        if (i%2 != 0)
            bufer += code.at(i).digitValue();
        else
            bufer += 3*code.at(i).digitValue();
    }
    crc = 10 - bufer%10;
    code.push_back(QString::number(crc, 10));
    ui.codeEdit->setText(code);

    countryRule.append("AAAA");
    countryRule.append("CCCC");
    ean8.append("101");
    for (int i=0; i < 8; ++i) {
        number = code.at(i).digitValue();
        if (countryRule.at(i) == 'A') {
            if (i == 4) ean8.append("01010");
            ean8.append(m_aCodesTable.at(number));
        }
        if (countryRule.at(i) == 'C') {
            if (i == 4) ean8.append("01010");
            ean8.append(m_cCodesTable.at(number));
        }
    }
    ean8.append("101");
    m_ean8 = ean8;
}

void Widget::startGenerating()
{
    if (ui.typeBox->currentText() == "EAN-13")
        calcEAN13(ui.codeEdit->text());
    if (ui.typeBox->currentText() == "EAN-8")
        calcEAN8(ui.codeEdit->text());
    drawImage();
    this->repaint();
    ui.saveButton->setEnabled(true);
}

void Widget::connectSignals()
{
    connect(ui.codeEdit, SIGNAL(returnPressed()), this, SLOT(startGenerating()));
    connect(ui.typeBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(setValidator(QString)));
    connect(ui.saveButton, SIGNAL(clicked()), this, SLOT(saveImage()));
}

void Widget::setValidator(const QString &codeType)
{
    if (codeType == "EAN-13") {
        ui.codeEdit->clear();
        ui.codeEdit->setMaxLength(13);
        QRegExp onlyNumbers("[0-9]{1,12}");
        ui.codeEdit->setValidator(new QRegExpValidator(onlyNumbers, this));
    }
    if (codeType == "EAN-8") {
        ui.codeEdit->clear();
        ui.codeEdit->setMaxLength(8);
        QRegExp onlyNumbers("[0-9]{1,7}");
        ui.codeEdit->setValidator(new QRegExpValidator(onlyNumbers, this));
    }
    ui.saveButton->setEnabled(false);
}

void Widget::setupTables()
{
    m_countryTable.append("AAAAAA");
    m_countryTable.append("AABABB");
    m_countryTable.append("AABBAB");
    m_countryTable.append("AABBBA");
    m_countryTable.append("ABAABB");
    m_countryTable.append("ABBAAB");
    m_countryTable.append("ABBBBA");
    m_countryTable.append("ABABAB");
    m_countryTable.append("ABABBA");
    m_countryTable.append("ABBABA");

    m_aCodesTable.append("0001101");
    m_aCodesTable.append("0011001");
    m_aCodesTable.append("0010011");
    m_aCodesTable.append("0111101");
    m_aCodesTable.append("0100011");
    m_aCodesTable.append("0110001");
    m_aCodesTable.append("0101111");
    m_aCodesTable.append("0111011");
    m_aCodesTable.append("0110111");
    m_aCodesTable.append("0001011");

    m_bCodesTable.append("0100111");
    m_bCodesTable.append("0110011");
    m_bCodesTable.append("0011011");
    m_bCodesTable.append("0100001");
    m_bCodesTable.append("0011101");
    m_bCodesTable.append("0111001");
    m_bCodesTable.append("0000101");
    m_bCodesTable.append("0010001");
    m_bCodesTable.append("0001001");
    m_bCodesTable.append("0010111");

    m_cCodesTable.append("1110010");
    m_cCodesTable.append("1100110");
    m_cCodesTable.append("1101100");
    m_cCodesTable.append("1000010");
    m_cCodesTable.append("1011100");
    m_cCodesTable.append("1001110");
    m_cCodesTable.append("1010000");
    m_cCodesTable.append("1000100");
    m_cCodesTable.append("1001000");
    m_cCodesTable.append("1110100");
}

void Widget::paintEvent(QPaintEvent *pe)
{
    Q_UNUSED(pe);
    QPainter painter;
    painter.begin(this);
    painter.drawPixmap(StartPoint, m_codePixmap);
    painter.end();
}

void Widget::saveImage()
{
    QString savePath = QFileDialog::getSaveFileName();
    if (!savePath.isNull()) {
        m_codePixmap.save(savePath + ".png", "png");
        saveFile(savePath);
    }
}

void Widget::drawImage()
{
    QPainter painter;
    painter.begin(&m_codePixmap);
    painter.initFrom(this);
    painter.eraseRect(rect());
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRect(0, 0, ImageSize.width(), ImageSize.height());
    brush.setColor(Qt::black);
    painter.setBrush(brush);
    painter.setPen(Qt::black);
    painter.setFont(QFont("DejaVu Sans Mono", 14));
    QSet<int> calibrate;
    QString leftCode;
    QString rightCode;
    if (ui.typeBox->currentText() == "EAN-8") {
        int x = 70;
        calibrate.insert(0);
        calibrate.insert(2);
        calibrate.insert(32);
        calibrate.insert(34);
        calibrate.insert(64);
        calibrate.insert(66);
        leftCode = ui.codeEdit->text();
        leftCode.remove(4, 4);
        for (int i=0; i < leftCode.count(); ++i)
            if (leftCode.at(i) != ' ')
                leftCode.insert(i+1, ' ');
        painter.drawText(90, 255, leftCode);
        rightCode = ui.codeEdit->text();
        rightCode.remove(0, 4);
        for (int i=0; i < rightCode.count(); ++i)
            if (rightCode.at(i) != ' ')
                rightCode.insert(i+1, ' ');
        painter.drawText(220, 255, rightCode);
        for (int i=0; i < m_ean8.count(); ++i) {
            if (m_ean8.at(i) == '1') {
                if (calibrate.contains(i))
                    painter.drawRect(x, 10, 3, 240);
                else
                    painter.drawRect(x, 10, 3, 220);
            }
            x += 4;
        }
    }
    if (ui.typeBox->currentText() == "EAN-13") {
        int x = 20;
        calibrate.insert(0);
        calibrate.insert(2);
        calibrate.insert(46);
        calibrate.insert(48);
        calibrate.insert(92);
        calibrate.insert(94);
        painter.drawText(5, 255, ui.codeEdit->text().at(0));
        leftCode = ui.codeEdit->text();
        leftCode.remove(0, 1);
        leftCode.remove(6, 6);
        for (int i=0; i < leftCode.count(); ++i)
            if (leftCode.at(i) != ' ')
                leftCode.insert(i+1, ' ');
        painter.drawText(40, 255, leftCode);
        rightCode = ui.codeEdit->text();
        rightCode.remove(0, 7);
        for (int i=0; i < rightCode.count(); ++i)
            if (rightCode.at(i) != ' ')
                rightCode.insert(i+1, ' ');
        painter.drawText(225, 255, rightCode);
        for (int i=0; i < m_ean13.count(); ++i) {
            if (m_ean13.at(i) == '1') {
                if (calibrate.contains(i))
                    painter.drawRect(x, 10, 3, 240);
                else
                    painter.drawRect(x, 10, 3, 220);
            }
            x += 4;
        }
    }
    painter.end();
}

void Widget::saveFile(QString savePath)
{
    QString codeString;
    QSet<int> calibrate;
    QFile file(savePath + ".hgl");
    QTextStream stream(&file);
    int x = 0;
    int r = 2;
    file.open(QIODevice::WriteOnly);
    stream.setCodec("Windows-1250 to 1258");
    stream << "IN;" << endl;
    stream << "SP1;" << endl;
    stream << "SC0,10,0,10;" << endl;
    stream << "PA;" << endl;
    if (ui.typeBox->currentText() == "EAN-8") {
        codeString = m_ean8;
        calibrate.insert(0);
        calibrate.insert(2);
        calibrate.insert(32);
        calibrate.insert(34);
        calibrate.insert(64);
        calibrate.insert(66);
    }
    if (ui.typeBox->currentText() == "EAN-13") {
        codeString = m_ean13;
        calibrate.insert(0);
        calibrate.insert(2);
        calibrate.insert(46);
        calibrate.insert(48);
        calibrate.insert(92);
        calibrate.insert(94);
    }
    for (int i=0; i < codeString.count(); ++i) {
        if (codeString.at(i) == '1') {
            if (calibrate.contains(i)) {
                stream << "PU" << x << "," << 0 << ";" << endl;
                stream << "RR" << 1 << "," << 7 << ";" << endl;
            } else {
                stream << "PU" << x << "," << 2 << ";" << endl;
                stream << "RR" << 1 << "," << 5 << ";" << endl;
            }
        }
        ++x;
    }
    file.close();
    file.setFileName(savePath + "circle.hgl");
    stream.setDevice(&file);
    file.open(QIODevice::WriteOnly);
    stream.setCodec("Windows-1250 to 1258");
    stream << "IN;" << endl;
    stream << "IP0,0,4000,4000;" << endl;
    stream << "SC0,300,0,300" << endl;
    stream << "PT1;" << endl;
    stream << "SP1;" << endl;
    for (int i=codeString.count()-1; i >= 0; --i) {
        if (codeString.at(i) == '1') {
            stream << "PU" << 0 << "," << 0 << ";" << endl;
            stream << "CI" << r << ";" << endl;
        }
        ++r;
    }
    file.close();
}
