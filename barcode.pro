QT += core gui

TARGET = barcode
TEMPLATE = app

SOURCES += \
    main.cpp \
    widget.cpp
HEADERS += \
    widget.h
FORMS += \
    widget.ui
