#ifndef WIDGET_H
#define WIDGET_H

#include "ui_widget.h"
#include <QtGui/QPixmap>

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *pe);

private:
    Ui::Widget ui;
    void calcEAN13(QString code);
    void calcEAN8(QString code);

    void setupTables();
    void connectSignals();
    void drawBarCode();
    void drawImage();
    void saveFile(QString savePath);

    QString m_ean13;
    QString m_ean8;
    QPixmap m_codePixmap;
    QList<QString> m_countryTable;
    QList<QString> m_aCodesTable;
    QList<QString> m_bCodesTable;
    QList<QString> m_cCodesTable;

private slots:
    void startGenerating();
    void setValidator(const QString &codeType);
    void saveImage();
};

#endif // WIDGET_H
